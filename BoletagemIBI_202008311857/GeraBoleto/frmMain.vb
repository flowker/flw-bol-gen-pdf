﻿Imports System.Configuration
Imports System.Threading
Imports EO.Pdf
Imports MySql.Data.MySqlClient

Public Class frmMain

#Region "Variáveis"
    Dim BD As New MySqlConnection(MySQL.GetStringConexao)
    Private _task_running As Boolean = False
    Private array_threads() As Threading.Thread
    Private Shared workers As New System.Collections.Generic.List(Of Boletagem_Worker)
    Private _bd As MySqlConnection
#End Region

#Region "Importação de Arquivo"
    Private Sub btnSelArquivo_Click(sender As Object, e As EventArgs) Handles btnSelArquivo.Click
        ofd.DefaultExt = "ret"
        ofd.Filter = "Arquivo MOD (*.txt)|*.txt"
        ofd.ShowDialog()

        If (ofd.FileName.Trim <> String.Empty) Then
            If (Strings.Left(New IO.FileInfo(ofd.FileName.Trim).Name, 3).ToUpper <> "MOD") Then
                MsgBox("Arquivo de carga inválido.", MsgBoxStyle.Critical)
            Else
                txtArquivo.Text = ofd.FileName.Trim
                btnProcessarArquivo.Enabled = True
                btnProcessarArquivo.Focus()
            End If
        End If
    End Sub

    Private Sub btnProcessarArquivo_Click(sender As Object, e As EventArgs) Handles btnProcessarArquivo.Click
        If (BD.State <> ConnectionState.Open) Then BD.Open()
        MySQL.ExecutaQuery("SET WAIT_TIMEOUT=99999;", BD)
        MySQL.EnableBulkInsert(BD)

        txtStatus.Text = $"Iniciando importação em {Now.ToString("dd/MM/yyyy à\s HH:mm:ss")}"

        Dim DirArquivo As String = ofd.FileName.Trim
        Dim ContLinha As Integer = 0
        Dim Arquivo As New IO.StreamReader(DirArquivo, System.Text.Encoding.Default)
        Dim Linha As String = Arquivo.ReadLine

        Dim CmdCadastroBoleto As New MySqlCommand("INSERT INTO Boletagem (NomeCredor, EnderecoCredor, CNPJCredor, LogoCartao, NomeDevedor, NumeroContrato, LogradouroDevedor, BairroDevedor, CidadeDevedor, UFDevedor, CEPDevedor, NumeroDocumentoDevedor, DescricaoTipoProduto, DiasAtraso, ValorPrincipalDebito, DataCalculo, ValorCorrigido, ValorEntradaAVista, ValorParcelaEntradaAVista, ValorTotalPlanoAVista, ValorDescontoAVista, Condicao1, ValorEntradaCondicao1, ValorParcelaCondicao1, ValorTotalPlanoCondicao1, Condicao2, ValorEntradaCondicao2, ValorParcelaCondicao2, ValorTotalPlanoCondicao2, Condicao3, ValorEntradaCondicao3, ValorParcelaCondicao3, ValorTotalPlanoCondicao3, DataVencimentoMala, LinhaDigitavel, CodigoBarras, DataValidadeMala, NossoNumeroBoleto, NumeroBancoBoleto, NomeBancoBoleto, CarteirabancoBoleto, NumeroCodigoCedenteBoleto, NumeroCartao, ValorPrincipal, DataBaseCalculo, DataOriginalAtraso, ValorDocumento, NumeroDocumento, NomeAssessoria, NumeroDocumentoAssessoria, HorarioAtendimentoAssessoria1, HorarioAtendimentoAssessoria2, EmailAssessoria, UFGrafica, FoneRetornoAssessoria1, FoneRetornoAssessoria2, FoneRetornoAssessoria3, FoneRetornoAssessoria4, FoneRetornoAssessoria5, LogradouroRemetente, BairroRemetente, CidadeRemetente, UFRemetente, CEPRemetente, CodigoAssessoria, DataHora) VALUES " &
                                                  "(@NomeCredor, @EnderecoCredor, @CNPJCredor, @LogoCartao, @NomeDevedor, @NumeroContrato, @LogradouroDevedor, @BairroDevedor, @CidadeDevedor, @UFDevedor, @CEPDevedor, @NumeroDocumentoDevedor, @DescricaoTipoProduto, @DiasAtraso, @ValorPrincipalDebito, @DataCalculo, @ValorCorrigido, @ValorEntradaAVista, @ValorParcelaEntradaAVista, @ValorTotalPlanoAVista, @ValorDescontoAVista, @Condicao1, @ValorEntradaCondicao1, @ValorParcelaCondicao1, @ValorTotalPlanoCondicao1, @Condicao2, @ValorEntradaCondicao2, @ValorParcelaCondicao2, @ValorTotalPlanoCondicao2, @Condicao3, @ValorEntradaCondicao3, @ValorParcelaCondicao3, @ValorTotalPlanoCondicao3, @DataVencimentoMala, @LinhaDigitavel, @CodigoBarras, @DataValidadeMala, @NossoNumeroBoleto, @NumeroBancoBoleto, @NomeBancoBoleto, @CarteirabancoBoleto, @NumeroCodigoCedenteBoleto, @NumeroCartao, @ValorPrincipal, @DataBaseCalculo, @DataOriginalAtraso, @ValorDocumento, @NumeroDocumento, @NomeAssessoria, @NumeroDocumentoAssessoria, @HorarioAtendimentoAssessoria1, @HorarioAtendimentoAssessoria2, @EmailAssessoria, @UFGrafica, @FoneRetornoAssessoria1, @FoneRetornoAssessoria2, @FoneRetornoAssessoria3, @FoneRetornoAssessoria4, @FoneRetornoAssessoria5, @LogradouroRemetente, @BairroRemetente, @CidadeRemetente, @UFRemetente, @CEPRemetente, @CodigoAssessoria, Now());", BD)

        With CmdCadastroBoleto.Parameters
            .Add("@NomeCredor", MySqlDbType.String)
            .Add("@EnderecoCredor", MySqlDbType.String)
            .Add("@CNPJCredor", MySqlDbType.String)
            .Add("@LogoCartao", MySqlDbType.String)
            .Add("@NomeDevedor", MySqlDbType.String)
            .Add("@NumeroContrato", MySqlDbType.String)
            .Add("@LogradouroDevedor", MySqlDbType.String)
            .Add("@BairroDevedor", MySqlDbType.String)
            .Add("@CidadeDevedor", MySqlDbType.String)
            .Add("@UFDevedor", MySqlDbType.String)
            .Add("@CEPDevedor", MySqlDbType.String)
            .Add("@NumeroDocumentoDevedor", MySqlDbType.String)
            .Add("@DescricaoTipoProduto", MySqlDbType.String)
            .Add("@DiasAtraso", MySqlDbType.String)
            .Add("@ValorPrincipalDebito", MySqlDbType.Decimal)
            .Add("@DataCalculo", MySqlDbType.Date)
            .Add("@ValorCorrigido", MySqlDbType.Decimal)
            .Add("@ValorEntradaAVista", MySqlDbType.Decimal)
            .Add("@ValorParcelaEntradaAVista", MySqlDbType.Decimal)
            .Add("@ValorTotalPlanoAVista", MySqlDbType.Decimal)
            .Add("@ValorDescontoAVista", MySqlDbType.Decimal)
            .Add("@Condicao1", MySqlDbType.String)
            .Add("@ValorEntradaCondicao1", MySqlDbType.Decimal)
            .Add("@ValorParcelaCondicao1", MySqlDbType.Decimal)
            .Add("@ValorTotalPlanoCondicao1", MySqlDbType.Decimal)
            .Add("@Condicao2", MySqlDbType.String)
            .Add("@ValorEntradaCondicao2", MySqlDbType.Decimal)
            .Add("@ValorParcelaCondicao2", MySqlDbType.Decimal)
            .Add("@ValorTotalPlanoCondicao2", MySqlDbType.Decimal)
            .Add("@Condicao3", MySqlDbType.String)
            .Add("@ValorEntradaCondicao3", MySqlDbType.Decimal)
            .Add("@ValorParcelaCondicao3", MySqlDbType.Decimal)
            .Add("@ValorTotalPlanoCondicao3", MySqlDbType.Decimal)
            .Add("@DataVencimentoMala", MySqlDbType.Date)
            .Add("@LinhaDigitavel", MySqlDbType.String)
            .Add("@CodigoBarras", MySqlDbType.String)
            .Add("@DataValidadeMala", MySqlDbType.Date)
            .Add("@NossoNumeroBoleto", MySqlDbType.String)
            .Add("@NumeroBancoBoleto", MySqlDbType.String)
            .Add("@NomeBancoBoleto", MySqlDbType.String)
            .Add("@CarteiraBancoBoleto", MySqlDbType.String)
            .Add("@NumeroCodigoCedenteBoleto", MySqlDbType.String)
            .Add("@NumeroCartao", MySqlDbType.String)
            .Add("@ValorPrincipal", MySqlDbType.Decimal)
            .Add("@DataBaseCalculo", MySqlDbType.Date)
            .Add("@DataOriginalAtraso", MySqlDbType.Date)
            .Add("@ValorDocumento", MySqlDbType.Decimal)
            .Add("@NumeroDocumento", MySqlDbType.String)
            .Add("@NomeAssessoria", MySqlDbType.String)
            .Add("@NumeroDocumentoAssessoria", MySqlDbType.String)
            .Add("@HorarioAtendimentoAssessoria1", MySqlDbType.String)
            .Add("@HorarioAtendimentoAssessoria2", MySqlDbType.String)
            .Add("@EmailAssessoria", MySqlDbType.String)
            .Add("@UFGrafica", MySqlDbType.String)
            .Add("@FoneRetornoAssessoria1", MySqlDbType.String)
            .Add("@FoneRetornoAssessoria2", MySqlDbType.String)
            .Add("@FoneRetornoAssessoria3", MySqlDbType.String)
            .Add("@FoneRetornoAssessoria4", MySqlDbType.String)
            .Add("@FoneRetornoAssessoria5", MySqlDbType.String)
            .Add("@LogradouroRemetente", MySqlDbType.String)
            .Add("@BairroRemetente", MySqlDbType.String)
            .Add("@CidadeRemetente", MySqlDbType.String)
            .Add("@UFRemetente", MySqlDbType.String)
            .Add("@CEPRemetente", MySqlDbType.String)
            .Add("@CodigoAssessoria", MySqlDbType.String)
        End With

        Dim NomeCredor, EnderecoCredor, CNPJCredor, NomeDevedor, NumeroContrato, LogradouroDevedor, BairroDevedor, CidadeDevedor, UFDevedor, CEPDevedor, NumeroDocumentoDevedor, DescricaoTipoProduto, LinhaDigitavel, CodigoBarras, NossoNumeroBoleto, NumeroBancoBoleto, NomeBancoBoleto, CarteiraBancoBoleto, NumeroCodigoCedenteBoleto, NumeroCartao, NumeroDocumento, NomeAssessoria, NumeroDocumentoAssessoria, HorarioAtendimentoAssessoria1, HorarioAtendimentoAssessoria2, EmailAssessoria, UFGrafica, FoneRetornoAssessoria1, FoneRetornoAssessoria2, FoneRetornoAssessoria3, FoneRetornoAssessoria4, FoneRetornoAssessoria5, LogradouroRemetente, BairroRemetente, CidadeRemetente, UFRemetente, CEPRemetente, CodigoAssessoria As String
        Dim LogoCartao, DiasAtraso, Condicao1, Condicao2, Condicao3 As Integer
        Dim ValorPrincipalDebito, ValorCorrigido, ValorEntradaAVista, ValorParcelaEntradaAVista, ValorTotalPlanoAVista, ValorDescontoAVista, ValorEntradaCondicao1, ValorParcelaEntradaCondicao1, ValorTotalPlanoCondicao1, ValorEntradaCondicao2, ValorParcelaEntradaCondicao2, ValorTotalPlanoCondicao2, ValorEntradaCondicao3, ValorParcelaEntradaCondicao3, ValorTotalPlanoCondicao3, ValorPrincipal, ValorDocumento As Decimal
        Dim DataCalculo, DataVencimentoMala, DataValidadeMala, DataBaseCalculo, DataOriginalAtraso As Date

        While Linha <> Nothing
            NomeCredor = Linha.Substring(0, 50).ToString.Trim
            EnderecoCredor = $"{Linha.Substring(121, 100).ToString.Trim} - {Linha.Substring(221, 50).ToString.Trim} - {Linha.Substring(271, 50).ToString.Trim}/{Linha.Substring(321, 2).ToString.Trim} - CEP: {Linha.Substring(323, 10).ToString.Trim}"
            CNPJCredor = Linha.Substring(333, 15).ToString.Trim
            LogoCartao = Integer.Parse(Linha.Substring(548, 50).ToString.Trim)
            NomeDevedor = Linha.Substring(748, 50).ToString.Trim
            NumeroContrato = Linha.Substring(798, 30).ToString.Trim
            LogradouroDevedor = $"{Linha.Substring(828, 42).ToString.Trim}, {Linha.Substring(870, 58).ToString.Trim}"
            BairroDevedor = Linha.Substring(928, 30).ToString.Trim
            CidadeDevedor = Linha.Substring(958, 30).ToString.Trim
            UFDevedor = Linha.Substring(988, 2).ToString.Trim
            CEPDevedor = Linha.Substring(990, 9).ToString.Trim
            NumeroDocumentoDevedor = Linha.Substring(999, 14).ToString.Trim
            DescricaoTipoProduto = Linha.Substring(1163, 50).ToString.Trim
            DiasAtraso = Integer.Parse(Linha.Substring(1213, 4).ToString.Trim)
            ValorPrincipalDebito = Integer.Parse(Linha.Substring(1217, 10).ToString.Trim) / 100
            DataCalculo = ExtraiDataAAAAMMDD(Linha.Substring(1227, 10).ToString.Trim)
            ValorCorrigido = Integer.Parse(Linha.Substring(1237, 10).ToString.Trim) / 100

            ValorEntradaAVista = Integer.Parse(Linha.Substring(1249, 14).ToString.Trim) / 100
            ValorParcelaEntradaAVista = Integer.Parse(Linha.Substring(1263, 10).ToString.Trim) / 100
            ValorTotalPlanoAVista = Integer.Parse(Linha.Substring(1273, 10).ToString.Trim) / 100
            ValorDescontoAVista = Integer.Parse(Linha.Substring(1313, 10).ToString.Trim) / 100

            Condicao1 = Linha.Substring(1323, 2).ToString.Trim
            ValorEntradaCondicao1 = Integer.Parse(Linha.Substring(1325, 14).ToString.Trim) / 100
            ValorParcelaEntradaCondicao1 = Integer.Parse(Linha.Substring(1339, 10).ToString.Trim) / 100
            ValorTotalPlanoCondicao1 = Integer.Parse(Linha.Substring(1349, 10).ToString.Trim) / 100

            Condicao2 = Linha.Substring(1399, 2).ToString.Trim
            ValorEntradaCondicao2 = Integer.Parse(Linha.Substring(1401, 14).ToString.Trim) / 100
            ValorParcelaEntradaCondicao2 = Integer.Parse(Linha.Substring(1415, 10).ToString.Trim) / 100
            ValorTotalPlanoCondicao2 = Integer.Parse(Linha.Substring(1425, 10).ToString.Trim) / 100

            Condicao3 = Linha.Substring(1475, 2).ToString.Trim
            ValorEntradaCondicao3 = Integer.Parse(Linha.Substring(1477, 14).ToString.Trim) / 100
            ValorParcelaEntradaCondicao3 = Integer.Parse(Linha.Substring(1491, 10).ToString.Trim) / 100
            ValorTotalPlanoCondicao3 = Integer.Parse(Linha.Substring(1501, 10).ToString.Trim) / 100

            DataVencimentoMala = ExtraiDataAAAAMMDD(Linha.Substring(1843, 10).ToString.Trim)

            LinhaDigitavel = Linha.Substring(1853, 54).ToString.Trim
            CodigoBarras = Linha.Substring(1907, 50).ToString.Trim

            DataValidadeMala = ExtraiDataAAAAMMDD(Linha.Substring(1957, 10).ToString.Trim)

            NossoNumeroBoleto = Linha.Substring(1967, 20).ToString.Trim
            NumeroBancoBoleto = Linha.Substring(1987, 4).ToString.Trim
            NomeBancoBoleto = Linha.Substring(1991, 20).ToString.Trim
            CarteiraBancoBoleto = Linha.Substring(2011, 4).ToString.Trim
            NumeroCodigoCedenteBoleto = Linha.Substring(2015, 20).ToString.Trim
            NumeroCartao = Linha.Substring(2035, 30).ToString.Trim

            ValorPrincipal = Integer.Parse(Linha.Substring(2255, 10).ToString.Trim) / 100
            DataBaseCalculo = ExtraiDataAAAAMMDD(Linha.Substring(2265, 10).ToString.Trim)
            DataOriginalAtraso = ExtraiDataAAAAMMDD(Linha.Substring(2275, 10).ToString.Trim)

            ValorDocumento = Integer.Parse(Linha.Substring(2285, 10).ToString.Trim) / 100
            NumeroDocumento = Linha.Substring(2295, 19).ToString.Trim

            NomeAssessoria = Linha.Substring(2674, 50).ToString.Trim
            NumeroDocumentoAssessoria = Linha.Substring(2724, 18).ToString.Trim
            HorarioAtendimentoAssessoria1 = Linha.Substring(2742, 20).ToString.Trim
            HorarioAtendimentoAssessoria2 = Linha.Substring(2762, 20).ToString.Trim
            EmailAssessoria = Linha.Substring(2782, 50).ToString.Trim
            UFGrafica = Linha.Substring(2832, 2).ToString.Trim
            FoneRetornoAssessoria1 = Linha.Substring(2834, 20).ToString.Trim
            FoneRetornoAssessoria2 = Linha.Substring(2854, 20).ToString.Trim
            FoneRetornoAssessoria3 = Linha.Substring(2874, 20).ToString.Trim
            FoneRetornoAssessoria4 = Linha.Substring(2894, 20).ToString.Trim
            FoneRetornoAssessoria5 = Linha.Substring(2914, 20).ToString.Trim
            LogradouroRemetente = Linha.Substring(2934, 100).ToString.Trim
            BairroRemetente = Linha.Substring(3034, 50).ToString.Trim
            CidadeRemetente = Linha.Substring(3084, 50).ToString.Trim
            UFRemetente = Linha.Substring(3134, 2).ToString.Trim
            CEPRemetente = Linha.Substring(3136, 10).ToString.Trim
            CodigoAssessoria = Linha.Substring(3296, 4).ToString.Trim

            With CmdCadastroBoleto.Parameters
                .Item("@NomeCredor").Value = NomeCredor
                .Item("@EnderecoCredor").Value = EnderecoCredor
                .Item("@CNPJCredor").Value = CNPJCredor
                .Item("@LogoCartao").Value = LogoCartao
                .Item("@NomeDevedor").Value = NomeDevedor
                .Item("@NumeroContrato").Value = NumeroContrato
                .Item("@LogradouroDevedor").Value = LogradouroDevedor
                .Item("@BairroDevedor").Value = BairroDevedor
                .Item("@CidadeDevedor").Value = CidadeDevedor
                .Item("@UFDevedor").Value = UFDevedor
                .Item("@CEPDevedor").Value = CEPDevedor
                .Item("@NumeroDocumentoDevedor").Value = NumeroDocumentoDevedor.PadLeft(11, "0")
                .Item("@DescricaoTipoProduto").Value = DescricaoTipoProduto
                .Item("@DiasAtraso").Value = DiasAtraso
                .Item("@ValorPrincipalDebito").Value = ValorPrincipalDebito
                .Item("@DataCalculo").Value = IIf(DataCalculo = New Date(1, 1, 1), DBNull.Value, DataCalculo)
                .Item("@ValorCorrigido").Value = ValorCorrigido
                .Item("@ValorEntradaAVista").Value = ValorEntradaAVista
                .Item("@ValorParcelaEntradaAVista").Value = ValorParcelaEntradaAVista
                .Item("@ValorTotalPlanoAVista").Value = ValorTotalPlanoAVista
                .Item("@ValorDescontoAVista").Value = ValorDescontoAVista
                .Item("@Condicao1").Value = Condicao1
                .Item("@ValorEntradaCondicao1").Value = ValorEntradaCondicao1
                .Item("@ValorParcelaCondicao1").Value = ValorParcelaEntradaCondicao1
                .Item("@ValorTotalPlanoCondicao1").Value = ValorTotalPlanoCondicao1
                .Item("@Condicao2").Value = Condicao2
                .Item("@ValorEntradaCondicao2").Value = ValorEntradaCondicao2
                .Item("@ValorParcelaCondicao2").Value = ValorParcelaEntradaCondicao2
                .Item("@ValorTotalPlanoCondicao2").Value = ValorTotalPlanoCondicao2
                .Item("@Condicao3").Value = Condicao3
                .Item("@ValorEntradaCondicao3").Value = ValorEntradaCondicao3
                .Item("@ValorParcelaCondicao3").Value = ValorParcelaEntradaCondicao3
                .Item("@ValorTotalPlanoCondicao3").Value = ValorTotalPlanoCondicao3
                .Item("@DataVencimentoMala").Value = IIf(DataVencimentoMala = New Date(1, 1, 1), DBNull.Value, DataVencimentoMala)
                .Item("@LinhaDigitavel").Value = LinhaDigitavel
                .Item("@CodigoBarras").Value = CodigoBarras
                .Item("@DataValidadeMala").Value = DataValidadeMala
                .Item("@NossoNumeroBoleto").Value = NossoNumeroBoleto
                .Item("@NumeroBancoBoleto").Value = NumeroBancoBoleto
                .Item("@NomeBancoBoleto").Value = NomeBancoBoleto
                .Item("@CarteiraBancoBoleto").Value = CarteiraBancoBoleto
                .Item("@NumeroCodigoCedenteBoleto").Value = NumeroCodigoCedenteBoleto
                .Item("@NumeroCartao").Value = NumeroCartao
                .Item("@ValorPrincipal").Value = ValorPrincipal
                .Item("@DataBaseCalculo").Value = IIf(DataBaseCalculo = New Date(1, 1, 1), DBNull.Value, DataBaseCalculo)
                .Item("@DataOriginalAtraso").Value = IIf(DataOriginalAtraso = New Date(1, 1, 1), DBNull.Value, DataOriginalAtraso)
                .Item("@ValorDocumento").Value = ValorDocumento
                .Item("@NumeroDocumento").Value = NumeroDocumento
                .Item("@NomeAssessoria").Value = NomeAssessoria
                .Item("@NumeroDocumentoAssessoria").Value = NumeroDocumentoAssessoria
                .Item("@HorarioAtendimentoAssessoria1").Value = HorarioAtendimentoAssessoria1
                .Item("@HorarioAtendimentoAssessoria2").Value = HorarioAtendimentoAssessoria2
                .Item("@EmailAssessoria").Value = EmailAssessoria
                .Item("@UFGrafica").Value = UFGrafica
                .Item("@FoneRetornoAssessoria1").Value = FoneRetornoAssessoria1
                .Item("@FoneRetornoAssessoria2").Value = FoneRetornoAssessoria2
                .Item("@FoneRetornoAssessoria3").Value = FoneRetornoAssessoria3
                .Item("@FoneRetornoAssessoria4").Value = FoneRetornoAssessoria4
                .Item("@FoneRetornoAssessoria5").Value = FoneRetornoAssessoria5
                .Item("@LogradouroRemetente").Value = LogradouroRemetente
                .Item("@BairroRemetente").Value = BairroRemetente
                .Item("@CidadeRemetente").Value = CidadeRemetente
                .Item("@UFRemetente").Value = UFRemetente
                .Item("@CEPRemetente").Value = CEPRemetente
                .Item("@CodigoAssessoria").Value = CodigoAssessoria
            End With

            CmdCadastroBoleto.ExecuteNonQuery()

            ContLinha += 1
            If (ContLinha Mod 5000 = 0) Then MySQL.CommmitBulkInsert(BD)

            Application.DoEvents()
            Linha = Arquivo.ReadLine
        End While

        MySQL.CommmitBulkInsert(BD)
        txtStatus.Text &= $"{vbNewLine}Concluída importação em {Now.ToString("dd/MM/yyyy à\s HH:mm:ss")}"
    End Sub
#End Region

#Region "Geração dos Boletos"
    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If (txtDiretorioPDF.Text = String.Empty) Then
            MsgBox("Informe o diretório para salvar os boletos gerados", MsgBoxStyle.Critical)
            btnSelDiretorioPDF.Focus()
        Else
            lblStatus.Text = "Iniciado"
            nupQtdeThreads.Enabled = False

            For i As Int32 = 0 To nupQtdeThreads.Value - 1
                Dim objBoletagem_worker As New Boletagem_Worker
                workers.Add(objBoletagem_worker)
            Next

            TimerGeracao.Enabled = True

            btnStart.Enabled = False
            btnPause.Enabled = True

            Application.DoEvents()
            Processa()
        End If
    End Sub

    Private Sub btnPause_Click(sender As System.Object, e As System.EventArgs) Handles btnPause.Click
        'Para timer e finaliza threads
        TimerGeracao.Enabled = False
        nupQtdeThreads.Enabled = True

        lblStatus.Text = "Pausado"

        btnStart.Enabled = True
        btnPause.Enabled = False
    End Sub

    <DebuggerStepThrough()>
    Private Sub TimerGeracao_Tick(sender As System.Object, e As System.EventArgs) Handles TimerGeracao.Tick
        If Not _task_running Then
            Try
                Processa()
            Catch ex As Exception
                MsgBox($"Erro ao iniciar o processamento: {ex.Message} em {Date.Now.ToString("dd/MM/yyyy à\s HH:mm:ss")}")
            End Try
        End If
    End Sub

    Private Sub Processa()
        _task_running = True
        _bd = New MySqlConnection(MySQL.GetStringConexao)
        _bd.Open()

        Using conDb As New MySqlConnection(MySQL.GetStringConexao)
            conDb.Open()
            lblEstats.Text = MySQL.ReadRegistro("SELECT COUNT(*) FROM Boletagem B WHERE Processado=0 ", conDb)
            conDb.Close()
        End Using

        For Each dwItem As DataRow In MySQL.DevolveDataTable("SELECT Id,RIGHT(NumeroDocumentoDevedor,3) AS Senha FROM Boletagem WHERE Processado=0 AND Executando=0 ORDER BY Id LIMIT " & nupQtdeThreads.Value, _bd).Rows
            lblStatus.Text = "Processando..."
            Application.DoEvents()

            For Each objWorker As Boletagem_Worker In workers 'Procura "worker" livre!
                If Not objWorker.Busy Then
                    lblEstats.Text -= 1

                    'Seta propriedades
                    objWorker.Busy = True
                    objWorker.IdBoletagem = dwItem("Id")
                    objWorker.PasswordPDF = dwItem("Senha")
                    objWorker.PDFDirectory = fbdSaveDirectory.SelectedPath
                    objWorker.URLServer = txtEnderecoServidor.Text

                    Dim Th As New Thread(AddressOf objWorker.Run)
                    With Th
                        .Name = Now
                        .IsBackground = True
                        .Start()
                    End With

                    _bd.Close()
                    Exit For
                End If
            Next
        Next

        If (_bd.State <> ConnectionState.Closed) Then _bd.Close()

        _task_running = False
    End Sub

    Private Sub btnSelDiretorioPDF_Click(sender As Object, e As EventArgs) Handles btnSelDiretorioPDF.Click
        fbdSaveDirectory.ShowDialog()
        If (fbdSaveDirectory.SelectedPath <> String.Empty) Then txtDiretorioPDF.Text = fbdSaveDirectory.SelectedPath
    End Sub
#End Region

#Region "Outras Opções"
    Private Sub btnRemoverMarcacaoProcessados_Click(sender As Object, e As EventArgs) Handles btnRemoverMarcacaoProcessados.Click
        If (MsgBox("Tem certeza que deseja remover a marcação de processados?", MsgBoxStyle.OkCancel + MsgBoxStyle.Question) = MsgBoxResult.Ok) Then
            If (BD.State <> ConnectionState.Open) Then BD.Open()
            MySQL.ExecutaQuery("UPDATE Boletagem SET Processado=0,GuidPDF=NULL,DataHoraProcessado=NULL,Executando=0 WHERE Processado=1", BD)
            MsgBox("Marcações removidas!", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnRemoverImportados_Click(sender As Object, e As EventArgs) Handles btnRemoverImportados.Click
        If (MsgBox("Tem certeza que deseja remover os registros importados?", MsgBoxStyle.OkCancel + MsgBoxStyle.Question) = MsgBoxResult.Ok) Then
            If (BD.State <> ConnectionState.Open) Then BD.Open()
            MySQL.ExecutaQuery("DELETE FROM Boletagem", BD)
            MsgBox("Registros removidos!", MsgBoxStyle.Information)
        End If
    End Sub
#End Region

#Region "Outros"
    Public Shared Function ExtraiDataAAAAMMDD(ByVal Data As String) As Date
        If (Data = String.Empty) Then Return New Date()
        Return New Date(Data.Substring(0, 4), Data.Substring(4, 2), Data.Substring(6, 2))
    End Function

    Private Sub Me_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        BD.Close()
    End Sub

    Private Sub btnSair_Click(sender As Object, e As EventArgs) Handles btnSair.Click
        Me.Close()
    End Sub
#End Region

End Class

#Region "Boletagem Worker"
Friend Class Boletagem_Worker
    Private con As MySqlConnection
    Private _busy As Boolean = False
    Private _IdBoletagem As Int32 = 0
    Private _DirectoryPDF As String
    Private _URLServer As String
    Private _PasswordPDF As String

    Public Property Busy() As Boolean
        Get
            Return _busy
        End Get
        Set(value As Boolean)
            _busy = value
        End Set
    End Property

    Public Property IdBoletagem() As Int32
        Get
            Return _IdBoletagem
        End Get
        Set(value As Int32)
            _IdBoletagem = value
        End Set
    End Property

    Public Property PasswordPDF() As String
        Get
            Return _PasswordPDF
        End Get
        Set(value As String)
            _PasswordPDF = value
        End Set
    End Property

    Public Property PDFDirectory() As String
        Get
            Return _DirectoryPDF
        End Get
        Set(value As String)
            _DirectoryPDF = value
        End Set
    End Property

    Public Property URLServer() As String
        Get
            Return _URLServer
        End Get
        Set(value As String)
            _URLServer = value
        End Set
    End Property

    Protected Friend Sub Run()
        con = New MySqlConnection(MySQL.GetStringConexao) : con.Open()

        MySQL.SetWaitTimeout(con)
        MySQL.ExecutaQuery("UPDATE Boletagem SET Executando=1 WHERE Id=" & _IdBoletagem, con) 'Processa rotina de boletagem...

        Try
            Dim PDFGuid As String = Guid.NewGuid.ToString
            Dim NomeArquivo As String = PDFGuid & ".pdf"
            Dim DocPDF As New PdfDocument

            DocPDF.Security.UserPassword = PasswordPDF
            DocPDF.Security.OwnerPassword = PasswordPDF

            EO.Pdf.Runtime.AddLicense(ConfigurationManager.AppSettings.Item("PDFKey"))
            EO.Pdf.HtmlToPdf.Options.PageSize = EO.Pdf.PdfPageSizes.A4
            EO.Pdf.HtmlToPdf.Options.OutputArea = New System.Drawing.RectangleF(0.2F, 0.2F, 8.1F, 12.0F)

            EO.Pdf.HtmlToPdf.Options.ZoomLevel = 1

            HtmlToPdf.ConvertUrl($"{URLServer}/GeraBoleto.aspx?Id=" & _IdBoletagem, DocPDF)
            DocPDF.Save(IO.Path.Combine(PDFDirectory, NomeArquivo))

            MySQL.ExecutaQuery("UPDATE Boletagem SET Processado=1,GuidPDF='" & PDFGuid & "',DataHoraProcessado=Now(),Executando=0 WHERE Id=" & _IdBoletagem, con)
        Catch ex As Exception
            MySQL.ExecutaQuery("UPDATE Boletagem SET Processado=0,Executando=0 WHERE Id=" & _IdBoletagem, con)
        End Try

        If MySQL.ReadRegistro("SELECT COUNT(*) FROM Boletagem WHERE Processado=0", con) = 0 Then
            MontaRelatorioFinal(con)
        End If

        Me.Busy = False
        con.Close()
    End Sub

    Protected Friend Sub MontaRelatorioFinal(con As MySqlConnection)
        Dim dwLote As DataRow = MySQL.DevolveDataTable("SELECT MIN(DataHoraProcessado) AS DataHoraInicio, MAX(DataHoraProcessado) AS DataHoraTermino,COUNT(*) AS Qtde FROM Boletagem", con).Rows(0)
        Dim Qtde As Integer = Integer.Parse(dwLote("Qtde")), DataHoraInicioBoletagem As DateTime = DateTime.Parse(dwLote("DataHoraInicio")), DataHoraFimBoletagem As DateTime = DateTime.Parse(dwLote("DataHoraTermino"))

        Dim strAnexoTXT As New System.Text.StringBuilder
        strAnexoTXT.Append("Id|GuidPDF|NumeroContrato|NumeroDocumentoDevedor" & vbNewLine)

        For Each dw As DataRow In MySQL.DevolveDataTable("SELECT Id,GuidPDF,NumeroContrato,NumeroDocumentoDevedor FROM Boletagem ORDER BY Id", con).Rows
            With strAnexoTXT
                .Append(dw("Id") & ";")
                .Append(dw("GuidPDF") & ".pdf;")
                .Append(dw("NumeroContrato") & ";")
                .Append(dw("NumeroDocumentoDevedor"))
                .Append(Environment.NewLine)
            End With
        Next

        If strAnexoTXT.Length <> 0 Then
            Dim SR As New IO.StreamWriter(IO.Path.Combine(PDFDirectory, "ResumoBoletagem_" & Now.ToString("ddMMyyHHmmssfff") & ".csv"), False, System.Text.Encoding.Default)
            SR.Write(strAnexoTXT.ToString)
            SR.Close()
        End If

        Dim Msg As String = $"Processamento concluído!{vbNewLine & vbNewLine}Total de registros processados: {Qtde}{vbNewLine}" &
                            $"Iniciado em {DataHoraInicioBoletagem.ToString("dd/MM/yy HH:mm:ss")}{vbNewLine}" &
                            $"Finalizado em {DataHoraFimBoletagem.ToString("dd/MM/yy HH:mm:ss")}{vbNewLine}" &
                            $"Duração total: {DataHoraFimBoletagem.Subtract(DataHoraInicioBoletagem).Hours} hora(s), {DataHoraFimBoletagem.Subtract(DataHoraInicioBoletagem).Minutes} minuto(s) e {DataHoraFimBoletagem.Subtract(DataHoraInicioBoletagem).Seconds} segundo(s)"

        MsgBox(Msg, MsgBoxStyle.Information)
    End Sub

End Class
#End Region