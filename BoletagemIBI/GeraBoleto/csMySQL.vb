﻿Imports MySql.Data.MySqlClient
Public Class MySQL
    Public Shared Function GetStringConexao() As String
        Return "server=flw_boleto_ibi.mysql.dbaas.com.br;port=3306;database=flw_boleto_ibi;User Id=flw_boleto_ibi;Password=Persus@2018;Connection Lifetime=20;Pooling=false;Min Pool Size=0;default command timeout=99999;"
    End Function

    Public Shared Function ExisteRegistro(ByVal SQL As String, ByVal BD As MySqlConnection) As Boolean
        If (BD.State <> ConnectionState.Open) Then BD.Open()
        Using Cmd As New MySqlCommand(SQL, BD)
            Return Not (IsNothing(Cmd.ExecuteScalar))
        End Using
    End Function

    Public Shared Sub ExecutaQuery(ByVal QuerySQL As String, ByVal BD As MySqlConnection)
        If (BD.State <> ConnectionState.Open) Then BD.Open()
        Using Cmd As New MySqlCommand(QuerySQL, BD)
            Cmd.ExecuteNonQuery()
        End Using
    End Sub

    Shared Function DevolveDataTable(ByVal SQL As String, ByVal Conexao As MySqlConnection) As DataTable
        Using DT As New DataTable
            Dim DA As New MySqlDataAdapter(SQL, Conexao)
            DA.Fill(DT)
            Return DT
        End Using
    End Function

    Shared Function ReadRegistro(ByVal SQL As String, ByVal BD As MySqlConnection) As String
        If (BD.State <> ConnectionState.Open) Then BD.Open()
        Using Cmd As New MySqlCommand(SQL, BD)
            Cmd.CommandTimeout = BD.ConnectionTimeout
            Return Cmd.ExecuteScalar
        End Using
    End Function

    Public Shared Sub EnableBulkInsert(BD As MySqlConnection)
        ExecutaQuery("COMMIT; SET FOREIGN_KEY_CHECKS=0; SET AUTOCOMMIT=0;", BD)
    End Sub

    Public Shared Sub CommmitBulkInsert(BD As MySqlConnection)
        ExecutaQuery("COMMIT;", BD)
    End Sub

    Public Shared Sub DisableBulkInsert(BD As MySqlConnection)
        ExecutaQuery("COMMIT; SET FOREIGN_KEY_CHECKS=1; SET AUTOCOMMIT=1;", BD)
    End Sub

    Public Shared Sub SetWaitTimeout(ByRef BD As MySqlConnection)
        MySQL.ExecutaQuery("SET wait_timeout = 5000;", BD)
    End Sub

End Class
