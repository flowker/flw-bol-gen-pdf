﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.fbdSaveDirectory = New System.Windows.Forms.FolderBrowserDialog()
        Me.TimerGeracao = New System.Windows.Forms.Timer(Me.components)
        Me.tbMain = New System.Windows.Forms.TabControl()
        Me.tpImportacao = New System.Windows.Forms.TabPage()
        Me.gbMain = New System.Windows.Forms.GroupBox()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.btnProcessarArquivo = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSelArquivo = New System.Windows.Forms.Button()
        Me.txtArquivo = New System.Windows.Forms.TextBox()
        Me.tpGeracao = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnSelDiretorioPDF = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDiretorioPDF = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtEnderecoServidor = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnPause = New System.Windows.Forms.Button()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblEstats = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nupQtdeThreads = New System.Windows.Forms.NumericUpDown()
        Me.tpAcoes = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnRemoverMarcacaoProcessados = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnRemoverImportados = New System.Windows.Forms.Button()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.tbMain.SuspendLayout()
        Me.tpImportacao.SuspendLayout()
        Me.gbMain.SuspendLayout()
        Me.tpGeracao.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nupQtdeThreads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpAcoes.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TimerGeracao
        '
        Me.TimerGeracao.Interval = 2500
        '
        'tbMain
        '
        Me.tbMain.Controls.Add(Me.tpImportacao)
        Me.tbMain.Controls.Add(Me.tpGeracao)
        Me.tbMain.Controls.Add(Me.tpAcoes)
        Me.tbMain.Location = New System.Drawing.Point(12, 16)
        Me.tbMain.Name = "tbMain"
        Me.tbMain.SelectedIndex = 0
        Me.tbMain.Size = New System.Drawing.Size(691, 278)
        Me.tbMain.TabIndex = 67
        '
        'tpImportacao
        '
        Me.tpImportacao.Controls.Add(Me.gbMain)
        Me.tpImportacao.Location = New System.Drawing.Point(4, 22)
        Me.tpImportacao.Name = "tpImportacao"
        Me.tpImportacao.Padding = New System.Windows.Forms.Padding(3)
        Me.tpImportacao.Size = New System.Drawing.Size(683, 252)
        Me.tpImportacao.TabIndex = 0
        Me.tpImportacao.Text = "Importação de Arquivos"
        Me.tpImportacao.UseVisualStyleBackColor = True
        '
        'gbMain
        '
        Me.gbMain.Controls.Add(Me.txtStatus)
        Me.gbMain.Controls.Add(Me.btnProcessarArquivo)
        Me.gbMain.Controls.Add(Me.Label1)
        Me.gbMain.Controls.Add(Me.btnSelArquivo)
        Me.gbMain.Controls.Add(Me.txtArquivo)
        Me.gbMain.Location = New System.Drawing.Point(6, 7)
        Me.gbMain.Name = "gbMain"
        Me.gbMain.Size = New System.Drawing.Size(665, 233)
        Me.gbMain.TabIndex = 51
        Me.gbMain.TabStop = False
        Me.gbMain.Text = "Arquivo de Carga"
        '
        'txtStatus
        '
        Me.txtStatus.Location = New System.Drawing.Point(6, 66)
        Me.txtStatus.Multiline = True
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtStatus.Size = New System.Drawing.Size(648, 153)
        Me.txtStatus.TabIndex = 58
        '
        'btnProcessarArquivo
        '
        Me.btnProcessarArquivo.Enabled = False
        Me.btnProcessarArquivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcessarArquivo.Location = New System.Drawing.Point(579, 37)
        Me.btnProcessarArquivo.Name = "btnProcessarArquivo"
        Me.btnProcessarArquivo.Size = New System.Drawing.Size(75, 23)
        Me.btnProcessarArquivo.TabIndex = 53
        Me.btnProcessarArquivo.Text = "&Importar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(3, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 52
        Me.Label1.Text = "Arquivo Fonte"
        '
        'btnSelArquivo
        '
        Me.btnSelArquivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSelArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelArquivo.ForeColor = System.Drawing.Color.Black
        Me.btnSelArquivo.Location = New System.Drawing.Point(541, 37)
        Me.btnSelArquivo.Name = "btnSelArquivo"
        Me.btnSelArquivo.Size = New System.Drawing.Size(32, 23)
        Me.btnSelArquivo.TabIndex = 51
        Me.btnSelArquivo.Text = "..."
        '
        'txtArquivo
        '
        Me.txtArquivo.BackColor = System.Drawing.Color.White
        Me.txtArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArquivo.Location = New System.Drawing.Point(6, 37)
        Me.txtArquivo.Name = "txtArquivo"
        Me.txtArquivo.ReadOnly = True
        Me.txtArquivo.Size = New System.Drawing.Size(537, 23)
        Me.txtArquivo.TabIndex = 50
        '
        'tpGeracao
        '
        Me.tpGeracao.Controls.Add(Me.GroupBox2)
        Me.tpGeracao.Controls.Add(Me.GroupBox1)
        Me.tpGeracao.Location = New System.Drawing.Point(4, 22)
        Me.tpGeracao.Name = "tpGeracao"
        Me.tpGeracao.Padding = New System.Windows.Forms.Padding(3)
        Me.tpGeracao.Size = New System.Drawing.Size(683, 252)
        Me.tpGeracao.TabIndex = 1
        Me.tpGeracao.Text = "Geração de Boletos"
        Me.tpGeracao.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnSelDiretorioPDF)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtDiretorioPDF)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtEnderecoServidor)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 126)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(665, 114)
        Me.GroupBox2.TabIndex = 69
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opções"
        '
        'btnSelDiretorioPDF
        '
        Me.btnSelDiretorioPDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSelDiretorioPDF.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelDiretorioPDF.ForeColor = System.Drawing.Color.Black
        Me.btnSelDiretorioPDF.Location = New System.Drawing.Point(625, 75)
        Me.btnSelDiretorioPDF.Name = "btnSelDiretorioPDF"
        Me.btnSelDiretorioPDF.Size = New System.Drawing.Size(32, 23)
        Me.btnSelDiretorioPDF.TabIndex = 72
        Me.btnSelDiretorioPDF.Text = "..."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Navy
        Me.Label6.Location = New System.Drawing.Point(6, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 13)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "Diretório PDF Boleto"
        '
        'txtDiretorioPDF
        '
        Me.txtDiretorioPDF.BackColor = System.Drawing.Color.White
        Me.txtDiretorioPDF.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiretorioPDF.Location = New System.Drawing.Point(10, 75)
        Me.txtDiretorioPDF.Name = "txtDiretorioPDF"
        Me.txtDiretorioPDF.ReadOnly = True
        Me.txtDiretorioPDF.Size = New System.Drawing.Size(618, 23)
        Me.txtDiretorioPDF.TabIndex = 70
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(6, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 13)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Endereço Servidor"
        '
        'txtEnderecoServidor
        '
        Me.txtEnderecoServidor.BackColor = System.Drawing.Color.White
        Me.txtEnderecoServidor.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEnderecoServidor.Location = New System.Drawing.Point(9, 34)
        Me.txtEnderecoServidor.Name = "txtEnderecoServidor"
        Me.txtEnderecoServidor.ReadOnly = True
        Me.txtEnderecoServidor.Size = New System.Drawing.Size(650, 23)
        Me.txtEnderecoServidor.TabIndex = 68
        Me.txtEnderecoServidor.Text = "https://localhost:44378"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnPause)
        Me.GroupBox1.Controls.Add(Me.btnStart)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblEstats)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblStatus)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.nupQtdeThreads)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(665, 110)
        Me.GroupBox1.TabIndex = 61
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Geração de Boletos"
        '
        'btnPause
        '
        Me.btnPause.Enabled = False
        Me.btnPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPause.Location = New System.Drawing.Point(139, 34)
        Me.btnPause.Name = "btnPause"
        Me.btnPause.Size = New System.Drawing.Size(75, 23)
        Me.btnPause.TabIndex = 72
        Me.btnPause.Text = "Pausar"
        Me.btnPause.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStart.Location = New System.Drawing.Point(58, 34)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 71
        Me.btnStart.Text = "Iniciar"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(6, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "Threads"
        '
        'lblEstats
        '
        Me.lblEstats.AutoSize = True
        Me.lblEstats.Location = New System.Drawing.Point(72, 84)
        Me.lblEstats.Name = "lblEstats"
        Me.lblEstats.Size = New System.Drawing.Size(0, 13)
        Me.lblEstats.TabIndex = 69
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 68
        Me.Label4.Text = "Pendentes:"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(53, 67)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 67
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Status:"
        '
        'nupQtdeThreads
        '
        Me.nupQtdeThreads.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nupQtdeThreads.Location = New System.Drawing.Point(9, 37)
        Me.nupQtdeThreads.Name = "nupQtdeThreads"
        Me.nupQtdeThreads.Size = New System.Drawing.Size(46, 20)
        Me.nupQtdeThreads.TabIndex = 65
        Me.nupQtdeThreads.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'tpAcoes
        '
        Me.tpAcoes.Controls.Add(Me.GroupBox3)
        Me.tpAcoes.Location = New System.Drawing.Point(4, 22)
        Me.tpAcoes.Name = "tpAcoes"
        Me.tpAcoes.Size = New System.Drawing.Size(683, 252)
        Me.tpAcoes.TabIndex = 2
        Me.tpAcoes.Text = "Ações"
        Me.tpAcoes.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.btnRemoverMarcacaoProcessados)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.btnRemoverImportados)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 7)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(665, 233)
        Me.GroupBox3.TabIndex = 69
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Ações "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(210, 33)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(454, 13)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "Ao executar essa ação, todos os registros processados serão reabertos para reproc" &
    "essamento."
        '
        'btnRemoverMarcacaoProcessados
        '
        Me.btnRemoverMarcacaoProcessados.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemoverMarcacaoProcessados.Location = New System.Drawing.Point(6, 19)
        Me.btnRemoverMarcacaoProcessados.Name = "btnRemoverMarcacaoProcessados"
        Me.btnRemoverMarcacaoProcessados.Size = New System.Drawing.Size(198, 40)
        Me.btnRemoverMarcacaoProcessados.TabIndex = 72
        Me.btnRemoverMarcacaoProcessados.Text = "&Remover Marcação ""Executando/Processado"""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(210, 79)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(339, 13)
        Me.Label7.TabIndex = 71
        Me.Label7.Text = "Ao executar essa ação, todos os registros importados serão excluídos."
        '
        'btnRemoverImportados
        '
        Me.btnRemoverImportados.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemoverImportados.Location = New System.Drawing.Point(6, 74)
        Me.btnRemoverImportados.Name = "btnRemoverImportados"
        Me.btnRemoverImportados.Size = New System.Drawing.Size(198, 23)
        Me.btnRemoverImportados.TabIndex = 69
        Me.btnRemoverImportados.Text = "&Remover Registros Importados"
        '
        'btnSair
        '
        Me.btnSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSair.Location = New System.Drawing.Point(638, 300)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(65, 23)
        Me.btnSair.TabIndex = 45
        Me.btnSair.Text = "Sai&r"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(707, 333)
        Me.Controls.Add(Me.tbMain)
        Me.Controls.Add(Me.btnSair)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Boletagem"
        Me.tbMain.ResumeLayout(False)
        Me.tpImportacao.ResumeLayout(False)
        Me.gbMain.ResumeLayout(False)
        Me.gbMain.PerformLayout()
        Me.tpGeracao.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nupQtdeThreads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpAcoes.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ofd As OpenFileDialog
    Friend WithEvents fbdSaveDirectory As FolderBrowserDialog
    Friend WithEvents TimerGeracao As Timer
    Friend WithEvents tbMain As TabControl
    Friend WithEvents tpImportacao As TabPage
    Friend WithEvents gbMain As GroupBox
    Friend WithEvents txtStatus As TextBox
    Friend WithEvents btnProcessarArquivo As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents btnSelArquivo As Button
    Friend WithEvents txtArquivo As TextBox
    Friend WithEvents tpGeracao As TabPage
    Friend WithEvents btnSair As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnSelDiretorioPDF As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents txtDiretorioPDF As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtEnderecoServidor As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnPause As Button
    Friend WithEvents btnStart As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents lblEstats As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblStatus As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents nupQtdeThreads As NumericUpDown
    Friend WithEvents tpAcoes As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents btnRemoverImportados As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents btnRemoverMarcacaoProcessados As Button
End Class
