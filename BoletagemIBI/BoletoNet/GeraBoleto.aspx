﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GeraBoleto.aspx.vb" Inherits="BoletoNet.GeraBoleto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .texto {
            font-family: Arial,sans-serif;
            font-size: 7.5pt;
            color: black;
        }
    </style>
</head>
<body>
    <form id="frmGeral" runat="server">
        <center>
        <table border="0" width="666px">
            <tr>
                <td class="texto">
                    <table border="0" class="texto" style="width:100%;height:100%;">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="lblLocalData" Font-Bold="true" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table border="0" class="texto" style="width:100%;height:100%;">
                        <tr>
                            <td style="width:40%">
                                <table border="0" class="texto">
                                    <tr>
                                        <td valign="middle">
                                            <center><img src="../Img/Logo.png" style="width: 280px;" /></center><br/>

                                            <b>BANCO BRADESCARD S.A.</b><br />
                                            CNPJ: 04.184.779/0001-01<br/>
                                            ALAMEDA RIO NEGRO, 585 ALPHAVILLE<br />
                                            BARUERI/SP - CEP: 06454-000<br /><br />

                                            <b>LIDERANÇA</b><br />
                                            CNPJ: 59.937.524/0001-56<br/>
                                            PRAÇA XV DE NOVEMBRO, 120 CENTRO<br />
                                            RIO DE JANEIRO/RJ - CEP: 20010-010<br /><br />

                                            <asp:Label ID="lblNomeSacado" Font-Bold="true" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblDocumentoSacado" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblEnderecoSacado" runat="server"></asp:Label><br />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:60%;">
                                <div style="border: 4px solid gray; padding: 10px; height:100%;">
                                    <table border="0" class="texto" style="width: 100%;height: 100%;font-size: 10pt;font-weight: bold;">
                                        <tr>
                                            <td>
                                                Cartão: <asp:Label ID="lblNumeroCartao" Font-Bold="true" runat="server"></asp:Label><br />
                                                Telefone de Contato: <asp:Label ID="lblTelefonePrincipalContato" Font-Bold="true" runat="server"></asp:Label><br />
                                                Proposta válida até: <asp:Label ID="lblDataValidadeProposta" Font-Bold="true" runat="server"></asp:Label><br/><br />
                                                Seu débito atualizado até: <asp:Label ID="lblDataDebitoAtualizado" runat="server"></asp:Label> é de <asp:Label ID="lblValorDebitoAtualizado" Font-Bold="true" runat="server"></asp:Label>.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: gray;">
                                                <div style="border: 2px solid gray; padding: 5px; width: 90%; text-align: center;">
                                                    <span style="font-size:10pt;">Vencimento: </span><br/><span style="font-size:16pt;"><asp:Label ID="lblDataVencimento" Font-Bold="true" runat="server"></asp:Label></span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="texto"><br/>
                    <b style="font-size:10pt;">NOTIFICAÇÃO EXTRAJUDICIAL</b><br/><br/>
                    <p align=”justify”>Apesar das oportunidades concediadas a V.Sa. nas campanhas por período determinado para LIQUIDAÇÃO do seu débito com o cartão <asp:Label ID="lblDescricaoProduto" Font-Bold="true" runat="server"></asp:Label>, cuja cobrança encontra-se atualmente sob nossa responsabilidade, LIDERANÇA, empresa contratada pelo BANCO BRADESCARD S.A. para execução dos serviços de cobrança, nossos registros indicam que na fase amigável não se obteve nenhuma solução,. Por essa razão, vimos NOTIFICAR para que nos contate, o mais rápido possível, ou pague o boileto abaixo, novamente com valores de campanha, a fim de que tal pendência seja solucionada ainda na fase conciliatória, evitando assim, que a Credora possa eventualmente ingressa com a competente Ação Judicial, cujo procedimento poderá trazer a V.Sa., dentre outras, consequências previstas no CÓDIGO DE PROCESSO CIVIL BRASILEIRO. O não atendimento das providências requeridas nesta NOTIFICAÇÃO, será interpretado como falta de interesse na realização de Acordo Amigável, o qual, como pode constatar abaixo, lhe trará EXCELENTES VANTAGENS por trata-se de valores de campanha, sendo, portanto, OPORTUNIDADE ÚNICA que não deve ser perdida.</p>
                </td>
            </tr>
            <tr>
                <td align="center" class="texto">
                    <div style="border-radius: 25px; border: 2px solid black; padding: 3px; width: 90%; margin-top:3pt; margin-bottom:3pt;">
                        <table border="0" class="texto" style="width:100%; font-size: 10pt; font-weight:bold;">
                            <tr>
                                <td>
                                    <center>
                                        Saldo Atualizado da Dívida<br/>
                                        <asp:Label ID="lblValorSaldoAtualizadoDivida" Font-Bold="true" runat="server"></asp:Label>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        Valor para Pagamento À Vista<br/>
                                        <asp:Label ID="lblValorPagamentoAVista" Font-Bold="true" runat="server"></asp:Label>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" class="texto">
                    <asp:Panel ID="pnCondicoesParcelas" runat="server" Visible="false">
                        <b>Opções de Pagamento Parcelado. (Válido somente se preenchido)</b><br/>

                        <asp:Repeater ID="rptCondicoesPagamento" runat="server">
                            <HeaderTemplate>
                                <table border="1" class="texto" style="border: 1px solid black; padding: 3px; font-size: 10pt; font-weight:bold; width: 70%; margin-top:3pt; margin-bottom:3pt;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <center><%# DataBinder.Eval(Container.DataItem, "DescricaoCondicao") %></center>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            <script>document.getElementById('lgDetalhe').onclick = selectCellText;</script>
                            </FooterTemplate>
                        </asp:Repeater>

                        <b>No plano parcelado, os boletos das demais parcelas serão disponibilizados pela assessoria.</b><br/>
                    </asp:Panel>

                    OBS: Caso já tenha regularizado este débito, ao receber esta NOTIFICAÇÃO, pedimos torná-la sem efeito.<br/>
                    Em caso de dúvida, eou necessitando de uma outra alternativa de negociação, contate-nos:<br/><br/>
                    <b>Telefone(s): <asp:Label ID="lblTelefonesContato" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    <br />
                    <table border="0" class="texto" style="width:100%; border-spacing: 10px;">
                        <tr>
                            <td colspan="3" style="padding: 3px;border-bottom: 1px solid black;border-right: 1px solid black;">Pagador<br />
                                <asp:Label ID="lblNomeCliente" Font-Bold="true" runat="server"></asp:Label>
                            </td>
                            <td style="padding: 3px;border-bottom: 1px solid black;border-right: 1px solid black;">Vencimento:<br />
                                <center><asp:Label ID="lblVcto" Font-Bold="true" runat="server"></asp:Label></center>
                            </td>
                            <td style="padding: 3px;border-bottom: 1px solid black;">(=) Valor do Documento:<br />
                                <center><asp:Label ID="lblValor" Font-Bold="true" runat="server"></asp:Label></center>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 3px;border-bottom: 1px solid black;border-right: 1px solid black;">Agência/Código do Cedente:<br />
                                <center><asp:Label ID="lblAgenciaCedente" Font-Bold="true" runat="server"></asp:Label></center>
                            </td>
                            <td style="padding: 3px;border-bottom: 1px solid black;border-right: 1px solid black;">Número do Documento:<br />
                                <center><asp:Label ID="lblNumeroDocumento" Font-Bold="true" runat="server"></asp:Label></center>
                            <td style="padding: 3px;border-bottom: 1px solid black;border-right: 1px solid black;">Nosso Número:<br />
                                <center><asp:Label ID="lblNossoNumero" Font-Bold="true" runat="server"></asp:Label></center>
                            </td>
                            <td style="padding: 3px;" colspan="2"><center>Autenticação Mecânica:</center><br />
                            </td>
                        </tr>
                    </table>
                    <hr style="border-top: 1px dashed; margin-top:2pt; margin-bottom:2pt;">
                </td>
            </tr>
        </table>
        <asp:Panel ID="panelBoleto" runat="server"></asp:Panel>
        </center>
    </form>
</body>
</html>
