﻿Imports MySql.Data.MySqlClient

Public Class GeraBoleto
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (Me.IsPostBack) Then
            If (IsNothing(Request.QueryString("Id")) Or Not IsNumeric(Request.QueryString("Id"))) Then
                Response.Write("Parâmetros 'Id' não informado ou inválido.")
                Response.End()
            End If

            Dim Id = Integer.Parse(Request.QueryString("Id"))
            Dim BD As New MySqlConnection(MySQL.GetStringConexao) : BD.Open()
            Dim dw As DataRow = MySQL.DevolveDataTable($"SELECT * FROM Boletagem WHERE Id={Id}", BD).Rows(0)

            Dim NomeCedente As String = dw("NomeCredor").ToString()
            Dim CNPJCedente As String = dw("CNPJCredor").ToString()
            Dim EnderecoCedente As String = dw("EnderecoCredor").ToString()
            Dim AgenciaCedente As String = dw("NumeroCodigoCedenteBoleto").ToString.Substring(0, 4)
            Dim ContaCedente As String = dw("NumeroCodigoCedenteBoleto").ToString.Substring(5, 7)
            Dim DigitoConta As String = dw("NumeroCodigoCedenteBoleto").ToString.Substring(13, 1)
            Dim CarteiraCedente As String = Integer.Parse(dw("CarteiraBancoBoleto").ToString()).ToString
            Dim NumeroDocumento As String = dw("NumeroDocumento").ToString()
            Dim NossoNumero As String = dw("CodigoBarras").SubString(25, 11) 'Obtém nosso número do código de barras pois no campo específico está sendo enviado composto com dígito verificador

#Region "Cria DT de Condições de Pagamento Parcelado"
            Dim DTCondicoes = New DataTable()
            Dim _col As New DataColumn
            Dim _row As System.Data.DataRow

            With _col
                .ColumnName = "DescricaoCondicao"
                .DataType = System.Type.GetType("System.String")
            End With

            DTCondicoes.Columns.Add(_col)
#End Region

            If (dw("Condicao1").ToString <> "0") Then
                _row = DTCondicoes.NewRow
                _row("DescricaoCondicao") = $"Parcelado em Entrada de {Decimal.Parse(dw("ValorEntradaCondicao1")).ToString("C")} + {Integer.Parse(dw("Condicao1").ToString)}x " &
                                            $"de {Decimal.Parse(dw("ValorParcelaCondicao1")).ToString("C")}"
                DTCondicoes.Rows.Add(_row)
            End If

            If (dw("Condicao2").ToString <> "0") Then
                _row = DTCondicoes.NewRow
                _row("DescricaoCondicao") = $"Parcelado em Entrada de {Decimal.Parse(dw("ValorEntradaCondicao2")).ToString("C")} + {Integer.Parse(dw("Condicao2").ToString)}x " &
                                            $"de {Decimal.Parse(dw("ValorParcelaCondicao2")).ToString("C")}"
                DTCondicoes.Rows.Add(_row)
            End If

            If (dw("Condicao3").ToString <> "0") Then
                _row = DTCondicoes.NewRow
                _row("DescricaoCondicao") = $"Parcelado em Entrada de {Decimal.Parse(dw("ValorEntradaCondicao3")).ToString("C")} + {Integer.Parse(dw("Condicao3").ToString)}x " &
                                            $"de {Decimal.Parse(dw("ValorParcelaCondicao3")).ToString("C")}"
                DTCondicoes.Rows.Add(_row)
            End If

            Dim DataVencimentoBoleto As Date = Date.Parse(dw("DataVencimentoMala")) '.AddDays(10)
            Dim ValorBoleto As Decimal

            If (DTCondicoes.Rows.Count <> 0) Then 'Se houver condição de pagamento parcelado...
                ValorBoleto = 0

                With rptCondicoesPagamento
                    .DataSource = DTCondicoes
                    .DataBind()
                End With

                pnCondicoesParcelas.Visible = True
            Else
                ValorBoleto = Decimal.Parse(dw("ValorDocumento").ToString)
            End If

            Dim NomeSacado As String = dw("NomeDevedor").ToString
            Dim NumeroDocumentoSacado = dw("NumeroDocumentoDevedor").ToString
            Dim EnderecoSacadoLogradouro As String = dw("LogradouroDevedor").ToString
            Dim EnderecoSacadoBairro As String = dw("BairroDevedor").ToString
            Dim EnderecoSacadoCidade As String = dw("CidadeDevedor").ToString
            Dim EnderecoSacadoUF As String = dw("UFDevedor").ToString
            Dim EnderecoSacadoCEP As String = dw("CEPDevedor").ToString

            Dim _Cedente = New Cedente(CNPJCedente, NomeCedente & " - CNPJ: " & CNPJCedente & "<br/>" & EnderecoCedente, AgenciaCedente, ContaCedente, DigitoConta)
            _Cedente.Codigo = ContaCedente 'Dependendo da carteira, é necessário informar o código do cedente (o banco que fornece)
            Dim _Boleto = New Boleto(DataVencimentoBoleto, ValorBoleto, CarteiraCedente, NossoNumero, _Cedente) 'Dados para preenchimento do boleto (data de vencimento, valor, carteira e nosso número)
            _Boleto.NumeroDocumento = NumeroDocumento 'Dependendo da carteira, é necessário o número do documento
            _Boleto.LocalPagamento = "PAGÁVEL PREFERENCIALMENTE EM QUALQUER AGÊNCIA DO BRADESCO"

            'Informa os dados do sacado
            _Boleto.Sacado = New Sacado(NomeSacado)
            _Boleto.Sacado.Nome = NomeSacado
            _Boleto.Sacado.CPFCNPJ = NumeroDocumentoSacado
            _Boleto.Sacado.Endereco.End = EnderecoSacadoLogradouro
            _Boleto.Sacado.Endereco.Bairro = EnderecoSacadoBairro
            _Boleto.Sacado.Endereco.Cidade = EnderecoSacadoCidade
            _Boleto.Sacado.Endereco.CEP = EnderecoSacadoCEP
            _Boleto.Sacado.Endereco.UF = EnderecoSacadoUF

            Dim Inf As New Instrucao_Santander()
            Inf.Descricao = $"<br/>CIP 244<br/>Sr(a) CAIXA: NÃO receber documento após {DataVencimentoBoleto.ToString("dd/MM/yyyy")}.<br/><br/>" &
                            $"Referente ao contrato nº: {dw("NumeroContrato").ToString}<br/>REALIZE O PAGAMENTO NO VALOR EXATO, INCLUSIVE CENTAVOS, SOB PENA DE NÃO SER RECONHECIDO O PAGAMENTO.<br/><br/>" &
                            $"Assessoria {dw("NomeAssessoria").ToString}, prestadora de serviço do Banco Bradesco. "
            _Boleto.Instrucoes.Add(Inf)
            _Boleto.EspecieDocumento = New EspecieDocumento_Bradesco(5) 'Espécie do Documento - [R] Recibo

            Dim _BoletoBancario As New BoletoBancario()
            _BoletoBancario.CodigoBanco = Integer.Parse(dw("NumeroBancoBoleto").ToString())
            _BoletoBancario.Boleto = _Boleto
            _BoletoBancario.Boleto.Valida()
            _BoletoBancario.MostrarComprovanteEntrega = False
            _BoletoBancario.OcultarReciboSacado = True
            _BoletoBancario.OcultarInstrucoes = True

            lblNomeCliente.Text = _BoletoBancario.Sacado.Nome
            lblVcto.Text = _BoletoBancario.Boleto.DataVencimento
            lblValor.Text = IIf(_BoletoBancario.Boleto.ValorBoleto = 0, String.Empty, _BoletoBancario.Boleto.ValorBoleto)
            lblAgenciaCedente.Text = $"{AgenciaCedente}/{ContaCedente}-{DigitoConta}"
            lblLocalData.Text = String.Format("{0} de {1} de {2}", Now.Day, MonthName(Now.Month), Now.Year)
            lblNomeSacado.Text = _BoletoBancario.Sacado.Nome
            lblDocumentoSacado.Text = $"CPF/CNPJ: {_BoletoBancario.Sacado.CPFCNPJ}"
            lblEnderecoSacado.Text = $"{_BoletoBancario.Sacado.Endereco.End} {_BoletoBancario.Sacado.Endereco.Bairro}<br/>" &
                                     $"{_BoletoBancario.Sacado.Endereco.Cidade}/{_BoletoBancario.Sacado.Endereco.UF} - CEP: {_BoletoBancario.Sacado.Endereco.CEP}"
            lblNumeroCartao.Text = dw("NumeroContrato").ToString
            lblTelefonePrincipalContato.Text = dw("FoneRetornoAssessoria1").ToString
            lblTelefonesContato.Text = $"{dw("FoneRetornoAssessoria1").ToString} | {dw("FoneRetornoAssessoria2").ToString} | {dw("FoneRetornoAssessoria3").ToString}"
            lblDataValidadeProposta.Text = _BoletoBancario.Boleto.DataVencimento
            lblDataDebitoAtualizado.Text = Date.Parse(dw("DataCalculo")).ToString("dd/MM/yyyy")
            lblValorDebitoAtualizado.Text = Decimal.Parse(dw("ValorCorrigido")).ToString("C")
            lblValorSaldoAtualizadoDivida.Text = Decimal.Parse(dw("ValorCorrigido")).ToString("C")
            lblValorPagamentoAVista.Text = Decimal.Parse(dw("ValorTotalPlanoAVista")).ToString("C")
            lblDataVencimento.Text = _BoletoBancario.Boleto.DataVencimento
            lblDescricaoProduto.Text = dw("DescricaoTipoProduto").ToString
            lblNumeroDocumento.Text = _BoletoBancario.Boleto.NumeroDocumento
            lblNossoNumero.Text = _BoletoBancario.Boleto.NossoNumero

            If (_BoletoBancario.Boleto.CodigoBarra.LinhaDigitavel <> dw("LinhaDigitavel").ToString Or _BoletoBancario.Boleto.CodigoBarra.Codigo <> dw("CodigoBarras").ToString) Then
                BD.Close()

                Response.Write($"Código de barras gerado ({_BoletoBancario.Boleto.CodigoBarra.Codigo}) não confere com código de barras enviado ({dw("CodigoBarras").ToString }) ou " &
                               $" linha digitável gerada ({_BoletoBancario.Boleto.CodigoBarra.LinhaDigitavel}) não confere com a linha digitável enviado ({dw("LinhaDigitavel").ToString }).")
                Response.End()
            End If

            If (panelBoleto.Controls.Count = 0) Then
                panelBoleto.Controls.Add(_BoletoBancario)
            End If

            BD.Close()
        End If
    End Sub

End Class